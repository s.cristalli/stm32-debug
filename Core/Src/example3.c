#include <stdint.h>

volatile uint32_t a = 1 << 20;
volatile uint64_t b;

int example3(void)
{
    b = a << 20;

    a = 1 << 20;

    b = a * a;

    while(1);
}
