extern huart2;

void example_usart() {
	HAL_UART_Transmit(&huart2, "Hello world!\n", 13, 1);
	HAL_Delay(1000);
}
