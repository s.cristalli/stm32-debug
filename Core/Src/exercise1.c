#include <stdint.h>

uint32_t multiply(uint32_t x, uint32_t y) {
	uint32_t result = 0;
	for(; x > 0; x--) {
		for(; y > 0; y--) {
			result += 1;
		}
	}
	return result;
}

void exercise1() {
	multiply(4,5); // 20?

	while(1);
}
