#include <stdlib.h>
#include <string.h>
#include <stdint.h>

uint32_t sum = 0;

void update_sum(uint32_t product) {
	uint32_t value = product;
	sum += value;
	return;
}


void breakpoints() {
	uint32_t a = 2;
	uint32_t b = 10;

	uint32_t product = a * b;

	update_sum(product);

	return;
}

void conditions() {
	uint8_t i = 0;
	for(; i < 50; i++) {
		update_sum(i);
	}
	return;
}

void variables() {
	uint32_t a = 4;
	a++;
	a = a << 1;

	char* string;
	string = (char*) malloc(a);
	memset(string, 'A', a);

	return;
}

void expressions() {
	uint32_t a = 0;
	uint32_t b = 0;

	a++;
	b++;
	a++;
	b++;

	return;
}

void intro() {
	breakpoints();
	conditions();
	variables();
	expressions();
}
