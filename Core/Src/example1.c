#define PI 3.14159
#define TWOPI 2 * PI

float radius(float circumference) {
	return circumference * (1 / TWOPI);
}

void example1()
{
	float c = 6.28318;
	float r = radius(c);

	while(1);
}
