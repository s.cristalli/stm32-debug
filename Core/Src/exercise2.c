	#include <stdint.h>

	#define integer uint16_t

	void swap(int* a, int* b)
	{
		*a = *b;
		*b = *a;
	}

	void sort(int arr[], int n)
	{
		int i, j, min_index;

		for (i = 0; i < n - 1; i++) {
			min_index = i;
			for (j = i + 1; j < n; j++) {
				if (arr[j] < arr[min_index])
					min_index = j;
			}

			swap(&arr[min_index], &arr[j]);
		}
	}

	int exercise2()
	{
		int arr[] = { 10, 23, 0, 5, 9, -1 };
		int n = sizeof(arr) / sizeof(integer);

		sort(arr, n);

		return 0;
	}
